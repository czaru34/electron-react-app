# App has been migrated manually to new repository: [https://gitlab.com/czaru34/dust-app](https://gitlab.com/czaru34/dust-app)

## How to install on Ubuntu/Debian

1. Install Node.js LTS - it is really important https://github.com/nodesource/distributions/blob/master/README.md#deb
2. Clone/download repo
3. ```npm install``` - to install all of the required dependencies
4. ```sudo npm run start-linux``` - run with sudo to let back-end server gain access to your Network Interface
5. Done.
