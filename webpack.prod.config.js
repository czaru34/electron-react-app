const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const { resolve } = require('path')
import * as webpack from 'webpack'

module.exports = [
	{
		mode: 'production',
		entry: './src/electron.ts',
		target: 'electron-main',
		resolve: {
			extensions: ['.tsx', '.ts', '.js'],
		},
		module: {
			rules: [{
				test: /\.ts$/,
				include: /src/,
				use: [{ loader: 'ts-loader' }]
			}],

		},
		output: {
			path: path.join(__dirname + '.src/prod'),
			filename: 'electron.js'
		},
		externals: {
			'knex': 'commonjs knex',
			//'sqlite3': 'commonjs sqlite3',
			'fs': 'commonjs fs'
		}
	},
	{
		mode: 'production',
		entry: './src/index.tsx',
		target: 'electron-renderer',
		resolve: {
			extensions: ['.tsx', '.ts', '.js'],
		},
		devtool: 'source-map',
		module: {
			rules: [
				{
					test: /\.css$/,
					use: ['style-loader', 'css-loader']
				}
			]
		},
		output: {
			path: __dirname + '.src/prod',
			filename: 'index.js',
			sourceMapFilename: 'index.map.js'
		},
		plugins: [
			new webpack.EnvironmentPlugin({
				NODE_ENV: 'production',
				DEBUG_PROD: false,
			}),
		]
	}
]