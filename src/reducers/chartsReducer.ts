import { parse } from 'path'
import * as chartjs from 'chart.js'
import {
    GET_LAST_60, GET_LAST_6,
    GET_LAST_DAY,
    GET_LAST_WEEK,
    GET_FROM_TO
} from '../helpers/fetch'
import {
    ChartsDispatchTypes,
    ADD_TAB,
    REMOVE_TAB,
    TOGGLE_TAB,
    FETCH_DATA_FROM_DB,
    ADD_DROPDOWN,
    REMOVE_DROPDOWN,
    DROPDOWN_SELECT,
    SET_LAST_FETCH_TYPE,
    CHANGE_DATA_COLOR,
    CHANGE_TAB_NAME
} from '../actions/ChartsActionTypes'
import * as rgb from '../helpers/colors'

export interface ReduxDropdown {
    selected: string
    options: string[]
}

export interface IChartsReduxState {
    id: number
    name: string
    data: chartjs.ChartData
    options: chartjs.ChartOptions
    lastFetchType: string
    selected: string[]
    dropdowns: ReduxDropdown[]
}

interface IchartsInit {
    toggled: string
    chartCount: number
    chartDataColors: rgb.chartDataColor[]
    tabs: IChartsReduxState[]
}

const dropdownOptionsInit: string[] =
    [
        'Temperature',
        'Humidity',
        'Pm10',
        'Pm25',
        'Pm100',
        'Particles03',
        'Particles05',
        'Particles10',
        'Particles25',
        'Particles50',
        'Particles100'
    ]

var options: chartjs.ChartOptions =
{
    plugins: {
        zoom: {
            pan: {
                enabled: true,
                mode: 'xy'
            },
            zoom: {
                enabled: true,
                mode: 'xy',
                drag: false,
                sensitivity: 1,

            },
        }
    },

    responsive: true,
    scales: {
        xAxes: [
            {
                ticks: {
                    display: true,
                    autoSkip: true,
                    beginAtZero: true,
                    fontColor: 'lightgreen',
                    maxTicksLimit: 1,
                    maxRotation: 0,
                    minRotation: 0
                },
                gridLines: {
                    zeroLineColor: 'rgba(255, 255, 255, 1)',
                    color: 'rgba(255, 255, 255, 0.3)'
                }
            }
        ],
        yAxes: [
            {
                ticks: {
                    display: true,
                    beginAtZero: true,
                    fontColor: 'lightgreen'
                },
                gridLines: {
                    zeroLineColor: 'rgba(255, 255, 255, 1)',
                    color: 'rgba(255, 255, 255, 0.3)'
                }
            }
        ]
    },
    legend: {
        labels: {
            fontColor: 'white'
        }
    },
    elements: {
        point: {
            radius: 2.5,
            pointStyle: 'rect'
        }
    }
}

const chartsInit: IchartsInit = {
    toggled: '0',
    chartCount: 4,
    chartDataColors: [
        {
            dataType: 'Temperature',
            color: { r: 34, g: 139, b: 34 }
        },
        {
            dataType: 'Humidity',
            color: { r: 10, g: 205, b: 255 }
        },
        {
            dataType: 'Pm10',
            color: { r: 255, g: 0, b: 0 }
        },
        {
            dataType: 'Pm25',
            color: { r: 220, g: 20, b: 60 }
        },
        {
            dataType: 'Pm100',
            color: { r: 240, g: 128, b: 128 }
        },
        {
            dataType: 'Particles03',
            color: { r: 255, g: 204, b: 0 }
        },
        {
            dataType: 'Particles05',
            color: { r: 204, g: 255, b: 51 }
        },
        {
            dataType: 'Particles10',
            color: { r: 255, g: 51, b: 204 }
        },
        {
            dataType: 'Particles25',
            color: { r: 0, g: 255, b: 255 }
        },
        {
            dataType: 'Particles50',
            color: { r: 153, g: 153, b: 102 }
        },
        {
            dataType:
                'Particles100',
            color: {
                r: 255, g: 255, b: 255
            }
        }
    ],
    tabs: [
        {
            id: 0,
            name: 'Temperature & humidity',
            data: { labels: [], datasets: [] },
            options: options,
            lastFetchType: GET_LAST_DAY,
            selected: ['Temperature', 'Humidity'],
            dropdowns: [
                {
                    selected: 'Temperature',
                    options: [
                        'Pm10',
                        'Pm25',
                        'Pm100',
                        'Particles03',
                        'Particles05',
                        'Particles10',
                        'Particles25',
                        'Particles50',
                        'Particles100'
                    ]
                },
                {
                    selected: 'Humidity',
                    options: [
                        'Pm10',
                        'Pm25',
                        'Pm100',
                        'Particles03',
                        'Particles05',
                        'Particles10',
                        'Particles25',
                        'Particles50',
                        'Particles100'
                    ]
                }
            ]
        },
        {
            id: 1,
            name: 'PM\'s',
            data: { labels: [], datasets: [] },
            options: options,
            lastFetchType: GET_LAST_DAY,
            selected: [
                'Pm10',
                'Pm25',
                'Pm100'
            ],
            dropdowns: [
                {
                    selected: 'Pm10',
                    options: [
                        'Temperature',
                        'Humidity',
                        'Particles03',
                        'Particles05',
                        'Particles10',
                        'Particles25',
                        'Particles50',
                        'Particles100'
                    ]
                },
                {
                    selected: 'Pm25',
                    options: [
                        'Humidity',
                        'Particles03',
                        'Particles05',
                        'Particles10',
                        'Particles25',
                        'Particles50',
                        'Particles100'
                    ]
                },
                {
                    selected: 'Pm100',
                    options: [
                        'Humidity',
                        'Particles03',
                        'Particles05',
                        'Particles10',
                        'Particles25',
                        'Particles50',
                        'Particles100'
                    ]
                }
            ]
        },
        {
            id: 2,
            name: 'Particles',
            data: { labels: [], datasets: [] },
            options: options,
            lastFetchType: GET_LAST_DAY,
            selected: [
                'Particles03',
                'Particles05',
                'Particles10',
                'Particles25',
                'Particles50',
                'Particles100'
            ],
            dropdowns: [
                {
                    selected: 'Particles03',
                    options: [
                        'Temperature',
                        'Humidity',
                        'Pm10',
                        'Pm25',
                        'Pm100'
                    ]
                },
                {
                    selected: 'Particles05',
                    options: [
                        'Temperature',
                        'Humidity',
                        'Pm10',
                        'Pm25',
                        'Pm100'
                    ]
                },
                {
                    selected: 'Particles10',
                    options: [
                        'Temperature',
                        'Humidity',
                        'Pm10',
                        'Pm25',
                        'Pm100'
                    ]
                },
                {
                    selected: 'Particles25',
                    options: [
                        'Temperature',
                        'Humidity',
                        'Pm10',
                        'Pm25',
                        'Pm100'
                    ]
                },
                {
                    selected: 'Particles50',
                    options: [
                        'Temperature',
                        'Humidity',
                        'Pm10',
                        'Pm25',
                        'Pm100'
                    ]
                },
                {
                    selected: 'Particles100',
                    options: [
                        'Temperature',
                        'Humidity',
                        'Pm10',
                        'Pm25',
                        'Pm100'
                    ]
                },
            ]
        },
        {
            id: 3,
            name: 'All data',
            data: { labels: [], datasets: [] },
            options: options,
            lastFetchType: GET_LAST_DAY,
            selected: [
                'Temperature',
                'Humidity',
                'Pm10',
                'Pm25',
                'Pm100',
                'Particles03',
                'Particles05',
                'Particles10',
                'Particles25',
                'Particles50',
                'Particles100'
            ],
            dropdowns: [
                {
                    selected: 'Temperature',
                    options: []
                },
                {
                    selected: 'Humidity',
                    options: []
                },
                {
                    selected: 'Pm10',
                    options: []
                },
                {
                    selected: 'Pm25',
                    options: []
                },
                {
                    selected: 'Pm100',
                    options: []
                },
                {
                    selected: 'Particles05',
                    options: []
                },
                {
                    selected: 'Particles10',
                    options: []
                },
                {
                    selected: 'Particles25',
                    options: []
                },
                {
                    selected: 'Particles50',
                    options: []
                },
                {
                    selected: 'Particles100',
                    options: []
                },
            ]
        },
    ]
}

const chartsReducer = (state: IchartsInit, action: ChartsDispatchTypes) => {
    console.log(state)
    switch (action.type) {
        case ADD_TAB: {
            let newChartCount = state.chartCount
            let newTabs = state.tabs
            newChartCount > 0 ? newChartCount++ : newChartCount = 1
            newTabs.push(
                {
                    id: newChartCount - 1,
                    name: 'Rename me',
                    data: { labels: [], datasets: [] },
                    options,
                    lastFetchType: GET_LAST_DAY,
                    selected: new Array<string>(),
                    dropdowns: new Array<ReduxDropdown>()
                })
            return {
                ...state,
                chartCount: newChartCount,
                tabs: newTabs
            }
        }
        case REMOVE_TAB: {
            let newTabs = state.tabs
            var fIndex: number = newTabs.map(el => { return el.id }).indexOf(action.id)
            newTabs.splice(fIndex, 1)
            let remappedTabs = newTabs.filter((val, index) => {
                if (val.id > fIndex)
                    val.id = index

                return val
            })
            let newChartCount = state.chartCount
            if (newChartCount >= 0)
                newChartCount--
            return {
                ...state,
                chartCount: newChartCount,
                tabs: remappedTabs
            }

        }
        case TOGGLE_TAB: {
            return {
                ...state,
                toggled: action.tab
            }
        }
        case CHANGE_TAB_NAME: {
            let newTabs = state.tabs
            newTabs[action.id].name = action.name
            return {
                ...state,
                tabs: newTabs
            }
        }
        case ADD_DROPDOWN: {
            let newTabs = state.tabs
            var selectable: string[] = dropdownOptionsInit.filter(option => !newTabs[action.id].selected.includes(option))
            console.log(`Here are selectable: ${selectable}`)
            if (selectable.length != 0 && newTabs.length < 11) {
                let tempSelectable: string = selectable.shift()
                newTabs[action.id].dropdowns.push({ selected: tempSelectable, options: selectable })
                newTabs[action.id].selected.push(tempSelectable)
                newTabs[action.id].dropdowns = newTabs[action.id].dropdowns.filter(item => {
                    item.options = selectable
                    return item
                })
            }
            return {
                ...state,
                tabs: newTabs
            }
        }
        case REMOVE_DROPDOWN: {
            let newTabs = state.tabs
            newTabs[action.id].dropdowns.pop()
            newTabs[action.id].selected.pop()
            var selectable: string[] = dropdownOptionsInit.filter(option => !newTabs[action.id].selected.includes(option))
            newTabs[action.id].dropdowns = newTabs[action.id].dropdowns.filter(item => {
                item.options = selectable
                return item
            })
            return {
                ...state,
                tabs: newTabs
            }
        }
        case DROPDOWN_SELECT: {
            let newTabs = state.tabs
            newTabs[action.id].dropdowns[action.index].selected = action.payload
            newTabs[action.id].selected[action.index] = action.payload
            var selectable: string[] = dropdownOptionsInit.filter(option => !newTabs[action.id].selected.includes(option))
            newTabs[action.id].dropdowns = newTabs[action.id].dropdowns.filter(item => {
                item.options = selectable
                return item
            })
            return {
                ...state,
                tabs: newTabs
            }
        }
        case FETCH_DATA_FROM_DB: {
            let newTabs = state.tabs
            newTabs[action.id].data = action.payload
            return {
                ...state,
                tabs: newTabs
            }
        }
        case SET_LAST_FETCH_TYPE: {
            console.log('Last fetch type: ' + action.payload)
            let newTabs = state.tabs
            newTabs[action.id].lastFetchType = action.payload
            return {
                ...state,
                tabs: newTabs
            }
        }
        case CHANGE_DATA_COLOR: {
            let newColors = state.chartDataColors
            newColors.forEach((color) => {
                if (color.dataType === action.payload.dataType) {
                    color.color = action.payload.color
                    return color
                }
            })
            return {
                ...state,
                chartDataColors: newColors
            }
        }
        default:
            return chartsInit
    }
}

export default chartsReducer