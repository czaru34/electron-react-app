import * as React from 'react'
import { Component } from 'react'
import { StatusComponent } from '../components/status'
import { connect } from 'react-redux'
import { AppState } from '../components/main'
import { StatusDispatchTypes } from '../actions/StatusActionTypes'
import { Dispatch } from 'redux'
import { disconnect } from 'process'
import { ThunkDispatch } from 'redux-thunk'
import { Connect } from '../actions/StatusActions'
class AppStatus extends Component<Props, StatusInternals.State> {

    constructor(props: any) {
        super(props)
        this.state = {
            status: null
        }
    }


    componentDidMount() {
        this.setState({
            status: this.props.status
        })
    }

    handleIpInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            ...this.state,
            status: {
                ...this.state.status,
                ip: event.target.value
            }
        })
    }

    handleConnect = () => {
        this.props.handleConnectClick(this.state.status.ip)
        console.log(this.props)
    }

    render() {
        return (
            <StatusComponent
                ip={this.props.status.ip}
                address={this.props.status.address}
                status={this.props.status.condition}
                statusStyle={this.props.status.statusStyle}
                onChange={this.handleIpInputChange}
                onClick={this.handleConnect}
            />
        )
    }
}

// ! Props
type Props = StateProps & DispatchProps & StatusInternals.Props

// * The type for the props provided by the parent component
export namespace StatusInternals {
    export interface Props {
        status: {
            ip?: string
            address?: string
            condition?: string
            statusStyle?: React.CSSProperties
        }
    }

    export interface State {
        status: {
            ip?: string
            address?: string
            condition?: string
            statusStyle?: React.CSSProperties
        }
    }
}

// * typeof props from redux store (The type for the props provided by mapStateToProps())
interface StateProps {
    status: {
        ip: string
        address: string
        condition: string
        statusStyle: React.CSSProperties
    }
}

// * typeof methods from redux store (The type for the props provided by mapDispatchToProps())

interface DispatchProps {
    handleConnectClick: (ip: string) => Promise<void>
}

function mapStateToProps(state: AppState): StateProps {
    return {
        status: {
            condition: state.status.condition,
            statusStyle: state.status.statusStyle,
            ip: state.status.ip,
            address: state.status.address
        }
    }
}

function mapDispatchToProps(dispatch: ThunkDispatch<AppState, {}, StatusDispatchTypes>): DispatchProps {
    return {
        handleConnectClick: (ip: string) => dispatch(Connect(ip))
    }

}

export const StatusContainer =
    connect<StateProps, DispatchProps>
        (mapStateToProps,
            mapDispatchToProps)
        (AppStatus)