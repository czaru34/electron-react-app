
import * as app from './app'
import { SampleController } from './controllers/sample.controller'
import * as ip from 'ip'

process.env['HOST'] = ip.address()
process.env['PORT'] = '80'


const application: app.App = new app.App(
    [new SampleController()],
    process.env['PORT'],
    process.env['HOST']
)
export default application