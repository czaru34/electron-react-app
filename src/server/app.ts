import * as express from 'express'
import * as cors from 'cors'
import * as helmet from 'helmet'
import * as compression from 'compression'
import * as bodyParser from 'body-parser'

export class App 
{
    public app: any
    public controllers: any []
    public port: number
    public ip: any
    constructor(controllers: any[], port: any, ip: any)
    {
        this.app = express()
        this.port = port
        this.ip = ip
        this.controllers = controllers
        this.initializeMiddlewares()
        this.initializeControllers()
    }

    private initializeMiddlewares()
    {
        this.app.use(cors())
        this.app.use(helmet())
        this.app.use(compression())
        this.app.use(bodyParser.urlencoded({extended: false}))
        this.app.use(bodyParser.json())
        this.app.use( 
            (error: any, request: express.Request, response: express.Response, next: express.NextFunction) => {
                console.error(error.stack)
                response.status(500).send('Something is broken.')
            }
        )    
    }

    private initializeControllers()
    {
        this.controllers.forEach((controller) => {
            this.app.use(
                '/', controller.router
            )
        })
    }

    public listen() 
    {
        this.app.listen(
            this.port,
            this.ip,
            () => {
                console.log(`App is running on: ip:${this.ip} port:${this.port}`)
            }
        )
    }
}

