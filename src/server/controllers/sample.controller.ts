import * as express from 'express'
import { pg } from '../database'
import SampleModel from '../models/sampleModel'

export class SampleController {
    public router = express.Router()

    constructor() {
        this.initializeRoutes()
    }

    public initializeRoutes() {
        this.router.post('/submit', this.submitSample)
        this.router.get('/fetch/all', this.samplesAll)
        this.router.get('/fetch/last', this.fetchLast)
        this.router.get('/fetch/sixtyminutes', this.fetchLastSixtyMinutes)
        this.router.get('/fetch/sixhours', this.fetchLastSixHours)
        this.router.get('/fetch/today', this.fetchToday)
        this.router.get('/fetch/week', this.fetchWeek)
        this.router.get('/fetch/fromto/:from/:to', this.fetchFromTo)
    }

    samplesAll = async (req: express.Request, res: express.Response) => {
        pg
            .select('*')
            .from('samples')
            .orderBy('timestamp', 'desc')
            .limit(100)
            .then(
                (lastRecord: any) => {
                    res.json(lastRecord)
                    res.status(200)
                }
            )
            .catch(
                (error: any) => {
                    res.json({ message: `There was an error while getting last 100 record: ${error}` })
                    res.status(500)
                }
            )
    }

    submitSample = async (req: any, res: express.Response) => {
        if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
            console.log('Object missing')
            console.log(req.body)
        } else {
            try {
                var data: Array<SampleModel> = req.body
                console.log(data[0])
                let labels = ['timestamp',
                    'Temperature',
                    'Humidity',
                    'Pm10',
                    'Pm25',
                    'Pm100',
                    'Particles03',
                    'Particles05',
                    'Particles10',
                    'Particles25',
                    'Particles50',
                    'Particles100',]
                data.forEach(element => {
                    var tempModel: SampleModel = new SampleModel(
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        ''
                    )
                    var tempElement: SampleModel = new SampleModel(
                        element.timestamp,
                        element.Temperature,
                        element.Humidity,
                        element.Pm10,
                        element.Pm25,
                        element.Pm100,
                        element.Particles03,
                        element.Particles05,
                        element.Particles10,
                        element.Particles25,
                        element.Particles50,
                        element.Particles100
                    )
                    labels.forEach(label => {
                        if (label === 'timestamp') {
                            //console.log(element.timestamp)
                            var match = tempElement.timestamp.match(/^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/)
                            if (!(match === null)) {
                                var hours = parseInt(match[4], 10)
                                var minutes = parseInt(match[5], 10)
                                var seconds = parseInt(match[6], 10)
                                var year = parseInt(match[1], 10)
                                var month = parseInt(match[2], 10) - 1
                                var day = parseInt(match[3], 10)

                                var dateToCheck = new Date()
                                dateToCheck.setFullYear(year, month, day)
                                dateToCheck.setHours(hours, minutes, seconds)
                                if (dateToCheck.getHours() === hours &&
                                    dateToCheck.getMinutes() === minutes &&
                                    dateToCheck.getSeconds() === seconds &&
                                    dateToCheck.getFullYear() === year &&
                                    dateToCheck.getMonth() === month &&
                                    dateToCheck.getDate() === day)
                                    tempModel['timestamp'] = tempElement.timestamp
                                else {
                                    console.log('regex fail')
                                    return false
                                }
                            } else {
                                console.log('match is a null')
                                return false
                            }
                        }
                        else
                            if (isNaN(Number(tempElement.getValue(label)))) {
                                console.log('is nan')
                                return false
                            }
                            else {
                                tempModel.setValue(label, tempElement.getValue(label))
                            }
                    })
                    pg('samples')
                        .insert({
                            timestamp: tempModel.timestamp,
                            Temperature: parseFloat(tempModel.Temperature),
                            Humidity: parseFloat(tempModel.Humidity),
                            Pm10: parseFloat(tempModel.Pm10),
                            Pm25: parseFloat(tempModel.Pm25),
                            Pm100: parseFloat(tempModel.Pm100),
                            Particles03: parseFloat(tempModel.Particles03),
                            Particles05: parseFloat(tempModel.Particles05),
                            Particles10: parseFloat(tempModel.Particles10),
                            Particles25: parseFloat(tempModel.Particles25),
                            Particles50: parseFloat(tempModel.Particles50),
                            Particles100: parseFloat(tempModel.Particles100)
                        })
                        .catch((error: any) => {
                            //res.json({ STATUS: 'ERROR', ERROR: JSON.stringify(error) })
                            console.log(error)
                        })

                })
            } catch (err) {
                console.log(err)
            }
        }
    }

    fetchLast = async (req: any, res: any) => {
        pg
            .select('*')
            .from('samples')
            .orderBy('timestamp', 'desc')
            .limit(1)
            .then(
                (lastRecord: any) => {
                    res.json(lastRecord)
                    console.log('LAST RECORD:' + JSON.stringify(lastRecord))
                }
            )
            .catch(
                (error: any) => {
                    res.json({ message: `There was an error while getting last saved record: ${error}` })
                    res.status(500)
                    console.log(error)
                }
            )
    }

    fetchLastSixtyMinutes = async (req: any, res: any) => {
        pg
            .select('*')
            .from('samples')
            .orderBy('timestamp', 'desc')
            .limit(1)
            .then(
                (lastRecord: any) => {
                    pg
                        .schema.raw(`SELECT * from samples where timestamp between DATETIME('${lastRecord[0].timestamp}', '-1 hour') and DATETIME('${lastRecord[0].timestamp}');`)
                        .then(
                            (last60minutes: any) => {
                                console.log(last60minutes)
                                res.json(last60minutes)
                                res.status(200)
                            }
                        )
                }
            )
            .catch(
                (error: any) => {
                    res.json({ message: `There was an error while getting last hour: ${error}` })
                    res.status(500)
                }
            )
    }

    fetchLastSixHours = async (req: any, res: any) => {
        pg
            .select('*')
            .from('samples')
            .orderBy('timestamp', 'desc')
            .limit(1)
            .then(
                (lastRecord: any) => {
                    pg
                        .schema.raw(`SELECT * from samples where timestamp between DATETIME('${lastRecord[0].timestamp}', '-6 hour') and DATETIME('${lastRecord[0].timestamp}');`)
                        .then(
                            (last6hours: any) => {
                                res.json(last6hours)
                                res.status(200)
                            }
                        )
                }
            )
            .catch(
                (error: any) => {
                    res.json({ message: `There was an error while getting last 6 hours: ${error}` })
                    res.status(500)
                }
            )
    }

    fetchToday = async (req: any, res: any) => {
        pg
            .select('*')
            .from('samples')
            .orderBy('timestamp', 'desc')
            .limit(1)
            .then(
                (lastRecord: any) => {
                    pg
                        .schema.raw(`SELECT * from samples where timestamp between DATETIME('${lastRecord[0].timestamp}', '-1 days') and DATETIME('${lastRecord[0].timestamp}');`)
                        .then(
                            (today: any) => {
                                res.json(today)
                                res.status(200)
                            }
                        )
                }
            )
            .catch(
                (error: any) => {
                    res.json({ message: `There was an error while getting last 24hours: ${error}` })
                    res.status(500)
                }
            )
    }


    fetchWeek = async (req: any, res: any) => {
        pg
            .select('*')
            .from('samples')
            .orderBy('timestamp', 'desc')
            .limit(1)
            .then(
                (lastRecord: any) => {
                    pg
                        .schema.raw(`SELECT * from samples where timestamp between DATETIME('${lastRecord[0].timestamp}', '-7 days') and DATETIME('${lastRecord[0].timestamp}');`)
                        .then(
                            (today: any) => {
                                res.json(today)
                                res.status(200)
                            }
                        )
                }
            )
            .catch(
                (error: any) => {
                    res.json({ message: `There was an error while getting last week: ${error}` })
                    res.status(500)
                }
            )
    }

    fetchFromTo = async (req: any, res: any) => {
        console.log(req.params.from + ' ' + req.params.to)
        if (req.params.from == req.params.to) {
            var from = req.params.from + ' 00:00:00'
            var to = req.params.to + ' 23:59:00'
            pg
                .schema
                .raw(`SELECT * from samples where timestamp between DATETIME('${from}') and DATETIME('${to}');`)
                .then(
                    (fromto: any) => {
                        res.json(fromto)
                        res.status(200)
                    }
                )
                .catch(
                    (error: any) => {
                        res.json({ message: `There was an error while getting from - to: ${error}` })
                        res.status(500)
                    }
                )
        } else if (ifGreaterDates(req.params.from, req.params.to)) {
            pg
                .schema
                .raw(`SELECT * from samples where timestamp between DATETIME('${req.params.from}') and DATETIME('${req.params.to}');`)
                .then(
                    (fromto: any) => {
                        res.json(fromto)
                        res.status(200)
                    }
                )
                .catch(
                    (error: any) => {
                        res.json({ message: `There was an error while getting from - to: ${error}` })
                        res.status(500)
                    }
                )
        } else { console.log('Not this time ;)') }

    }
}

function ifGreaterDates(date0: Date, date1: Date) { return (date0 > date1 ? false : true) }
