import * as path from 'path'
import { CreateTableBuilder, AlterTableBuilder } from 'knex'
import fs from 'fs'

const databasePath = path.resolve(__dirname, './database.sqlite')

try {
    if (!fs.existsSync(databasePath)) {
        fs.open('database.sqlite', 'w', (err, file) => {
            if (err) throw err
            console.log('Created database file.')
        })
    }
} catch (err) {
    console.error(err)
}

const pg = require('knex')(
    {
        client: 'sqlite3',
        connection: {
            filename: databasePath
        },
        useNullAsDefault: true
    }
)

pg.schema
    .hasTable('samples')
    .then(
        (exists: boolean) => {
            if (!exists) {
                return pg.schema.createTable('samples', (table: CreateTableBuilder) => {
                    table.dateTime('timestamp', { useTz: true }).notNullable().primary()
                    table.float('Temperature')
                    table.float('Humidity')
                    table.float('Pm10')
                    table.float('Pm25')
                    table.float('Pm100')
                    table.float('Particles03')
                    table.float('Particles05')
                    table.float('Particles10')
                    table.float('Particles25')
                    table.float('Particles50')
                    table.float('Particles100')
                })
                    .then(
                        console.log('Done creating.')
                    )
                    .catch(
                        (error: any) => {
                            console.error(`There was an error creating table: ${error}`)
                        }
                    )
            }
        }
    )
    .then(
        () => { console.log('Done.') }
    ).catch(
        (error: any) => {
            console.error(`dbpath: ${databasePath}`)
            console.error(`There was an error setting up the database: ${error}`)
        }
    )

export { pg }