export const GET_LAST_60 = 'GET_LAST_60'
export const GET_LAST_6 = 'GET_LAST_6'
export const GET_LAST_DAY = 'GET_LAST_DAY'
export const GET_LAST_WEEK = 'GET_LAST_WEEK'
export const GET_FROM_TO = 'GET_FROM_TO'

