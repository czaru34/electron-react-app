

export type rgba = {
    r: number
    g: number
    b: number
    a?: number
}

export interface chartDataColor {
    dataType: string
    color: rgba
}

export function getRgbaString(color: rgba) { return `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a === undefined ? 1 : color.a})` }

export function rainbow() {
    var r = () => Math.random() * 256 >> 0
    return `rgb(${r()}, ${r()}, ${r()})`
}

