import { Dispatch } from 'redux'
import { STATUS_CONNECT, StatusDispatchTypes, STATUS_DISCONNECT } from './StatusActionTypes'
import { address } from 'ip'

export const Connect = (ip: string) => {
    return async (dispatch: Dispatch<StatusDispatchTypes>) => {
        try {
            var splittedString = ip.split('.')
            const between = (block: string) => parseInt(block) >= 0 && parseInt(block) <= 255
            if (splittedString.every(between)) {
                await fetch('http://' + ip + ':80/setAppIp', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        ip: address()
                    })
                })
                    .then((response: Response) => {
                        console.log(response.status)
                        if (response.status === 201) {
                            dispatch({ type: STATUS_CONNECT, ip: ip })
                        } else {
                            dispatch({ type: STATUS_DISCONNECT, ip: ip })
                        }
                    })
            } else {
                alert('invalid ip address')
                dispatch({ type: STATUS_DISCONNECT, ip: ip })
            }
        } catch (e: any) {

        }

    }
}