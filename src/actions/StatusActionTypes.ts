export const STATUS_CONNECT = 'STATUS_CONNECT'
export const STATUS_DISCONNECT = 'STATUS_DISCONNECT'

export interface Connect {
    type: typeof STATUS_CONNECT
    ip: string
}
export interface Disconnect {
    type: typeof STATUS_DISCONNECT
    ip: string
}

export type StatusDispatchTypes = Connect | Disconnect