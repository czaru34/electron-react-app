import { Dispatch } from 'redux'
import { ADD_TAB, REMOVE_TAB, ChartsDispatchTypes, TOGGLE_TAB, FETCH_DATA_FROM_DB, ADD_DROPDOWN, REMOVE_DROPDOWN, DROPDOWN_SELECT, SET_LAST_FETCH_TYPE, CHANGE_DATA_COLOR, CHANGE_TAB_NAME } from './ChartsActionTypes'
import {
    GET_LAST_60, GET_LAST_6,
    GET_LAST_DAY,
    GET_LAST_WEEK,
    GET_FROM_TO
} from '../helpers/fetch'
import axios from 'axios'
import { address } from 'ip'
import Sample from '../components/sampleDefinition'
import * as chartjs from 'chart.js'
import * as rgb from '../helpers/colors'

export const AddTab = (id: number) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: ADD_TAB })
    }
}

export const RemoveTab = (id: number) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: REMOVE_TAB, id })
    }
}

export const ChangeTabName = (id: number, name: string) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: CHANGE_TAB_NAME, id, name })
    }
}

export const Toggle = (tab: string) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: TOGGLE_TAB, tab })
    }
}

export const AddDropdown = (id: number) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: ADD_DROPDOWN, id })
    }
}

export const RemoveDropdown = (id: number) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: REMOVE_DROPDOWN, id })
    }
}

export const DropdownSelect = (id: number, index: number, payload: string) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: DROPDOWN_SELECT, id: id, index: index, payload: payload })
    }
}

export const Fetch = (id: number, selected: string[], typeOfFetch: string, chartDataColors: rgb.chartDataColor[], from?: string, to?: string) => {
    console.log(selected)
    let url: string = ''
    switch (typeOfFetch) {
        case GET_LAST_60: {
            url = 'http://' + address() + ':80/fetch/sixtyminutes'
        }
            break
        case GET_LAST_6: {
            url = 'http://' + address() + ':80/fetch/sixhours'
        }
            break
        case GET_LAST_DAY: {
            url = 'http://' + address() + ':80/fetch/today'
        }
            break
        case GET_LAST_WEEK: {
            url = 'http://' + address() + ':80/fetch/week'
        }
            break
        case GET_FROM_TO: {
            url = 'http://' + address() + `:80/fetch/fromto/${from}/${to}`
        }
            break
        default: {
            url = 'http://' + address() + ':80/fetch/today'
        }
            break
    }

    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        var promise: Promise<void> = axios
            .get(url)
            .then(response => {
                var data: chartjs.ChartData
                let newSamples: Sample[] = response.data
                var newLabels: string[] = []
                var newData: number[] = []
                var newDatasets: Object[] = []
                var i: number = 0

                selected.forEach((dataType: string, index: number) => {
                    newSamples.forEach((sample: Sample, index1: number) => {
                        let newSamplePart = sample[dataType]
                        newData[index1] = parseInt(newSamplePart)
                    })
                    let color = chartDataColors.filter((element) => {
                        if (element.dataType === dataType)
                            return element
                    })
                    newDatasets.push({
                        label: dataType.valueOf(),
                        data: newData,
                        borderColor: rgb.getRgbaString(color[0].color),
                        lineTension: 0,
                        fill: false
                    })
                    newData = []
                    i = 0
                })

                i = 0
                newSamples.forEach((sample: Sample) => {
                    newLabels[i] = sample['timestamp']
                    i++
                })
                data = { labels: newLabels, datasets: newDatasets }
                dispatch({ type: FETCH_DATA_FROM_DB, id, payload: data })
            })
            .catch(error => {
                console.error(`There was an error while ${typeOfFetch}: ${error}`)
            })
    }
}

export const SetLastFetchType = (id: number, typeOfFetch: string) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: SET_LAST_FETCH_TYPE, id, payload: typeOfFetch })
    }
}

export const ChangeDataColor = (color: rgb.chartDataColor) => {
    return (dispatch: Dispatch<ChartsDispatchTypes>) => {
        dispatch({ type: CHANGE_DATA_COLOR, payload: color })
    }
}