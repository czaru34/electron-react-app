import * as React from 'react'
import {
	Container,
	Row,
	Col,
	Popover,
	PopoverBody,
	PopoverHeader,
	Form,
	Input,
	Button
} from 'reactstrap'
import { BsFillXSquareFill, BsFillPlusSquareFill } from 'react-icons/bs'

namespace TabNameInternals {
	export interface Props {
		id: number
		name: string
		removeTab: (id: number) => void
		changeTabName: (id: number, name: string) => void
	}

	export interface State {
		popoverOpen: boolean
		name: string
	}
}

export default class TabName extends React.Component<TabNameInternals.Props, TabNameInternals.State> {

	timer: NodeJS.Timeout
	clickControl: boolean

	constructor(props: any) {
		super(props)
		this.clickControl = false
		this.state = {
			popoverOpen: false,
			name: this.props.name
		}
	}

	onNameDoubleClick = () => {
		this.setState({
			popoverOpen: !this.state.popoverOpen
		})
		console.log('Clicked twice!')
	}

	handleNameInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			name: event.target.value
		})
	}

	handleChange = () => {
		this.setState({ popoverOpen: false })
		this.props.changeTabName(this.props.id, this.state.name)
	}

	render() {
		return (
			<div>
				<div
					id={`c${this.props.id.toString()}`}
					key={this.props.name}
					className="tab-name"
					onDoubleClick={this.onNameDoubleClick}>
					<Popover
						placement='bottom'
						isOpen={this.state.popoverOpen}
						target={`c${this.props.id.toString()}`}
					>
						<PopoverHeader>
							Change name of the tab
                        </PopoverHeader>
						<PopoverBody>
							<Container>
								<Row className="justify-content-md-center">
									<Col>
										<Form>
											<Input type="text" onChange={this.handleNameInputChange} defaultValue={this.props.name} />
											<Button color='danger' onClick={() => this.setState({ popoverOpen: false })}>Close</Button>
											<Button color='success' onClick={this.handleChange}>Change</Button>
										</Form>
									</Col>
								</Row>
							</Container>
						</PopoverBody>
					</Popover>
					{this.props.name}
				</div>
				<span
					id={this.props.id.toString()}
					onClick={() => this.props.removeTab(this.props.id)}
					className="tab-close">
					<BsFillXSquareFill />
				</span>
			</div>
		)
	}
}