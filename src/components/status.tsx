import * as React from 'react'
import { Container, Row, Col, Form, Input, Button } from 'reactstrap/'

export interface StatusProps {
    ip: string
    address: string
    status: string
    statusStyle: React.CSSProperties
    onChange(e: React.ChangeEvent<HTMLInputElement>): void
    onClick(): void
}
export const StatusComponent = (props: StatusProps) => {

    const [inputValue, setInputValue] = React.useState(props.ip)

    return (
        <Row className="justify-content-center margin-zero-auto">
            <Container className="status-bigger-text">
                <Row className="justify-content-md-center status-element">
                    <Col md lg="3">
                        Server IP address:
                        </Col>
                    <Col md lg="3">
                        {props.address}
                    </Col>
                </Row>
                <Row className="justify-content-md-center status-element">
                    <Col md lg="3">
                        Connection status:
                        </Col>
                    <Col md lg="3" style={props.statusStyle}>
                        {props.status}
                    </Col>
                </Row>
                <Row className="justify-content-md-center status-element">
                    <Col md lg="3">
                        Connect to device <br></br>(enter IP address of the device):
                        </Col>
                    <Col md lg="3">
                        <Form>
                            <Input type="text" onChange={props.onChange} defaultValue={inputValue} />
                            <Button color='success' className="margin-1" onClick={props.onClick} >Connect</Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </Row >
    )
}
