import * as React from 'react'
import { Container, Row, Col, Button, ButtonGroup } from 'reactstrap'
import {
    GET_LAST_60, GET_LAST_6,
    GET_LAST_DAY,
    GET_LAST_WEEK,
    GET_FROM_TO
} from '../helpers/fetch'
import DataDropdown from './datadropdown'
import DateTime from 'react-datetime'
import * as chartjs from 'chart.js'
import 'chartjs-plugin-zoom'
import { Line } from 'react-chartjs-2'
import { ReduxDropdown } from '../reducers/chartsReducer'
import * as moment from 'moment'
import 'hammerjs'
import * as rgb from '../helpers/colors'
import ColorSettings from './colorsettings'


interface IChartPanel {
    chartDataColors: rgb.chartDataColor[],
    id: number
    data: chartjs.ChartData
    options: chartjs.ChartOptions
    lastFetchType: string
    selected: string[]
    dropdowns: Array<ReduxDropdown>
    addDropdown: (id: number) => void
    removeDropdown: (id: number) => void
    dropdownSelect: (id: number, index: number, payload: string) => void
    fetch: (id: number, selected: string[], typeOfFetch: string, chartDataColors: rgb.chartDataColor[], from?: string, to?: string) => void
    setLastFetchType: (id: number, typeOfFetch: string) => void
    changeDataColor: (color: rgb.chartDataColor) => void
}


export const ChartPanel = (props: IChartPanel) => {
    const [id, setId] = React.useState(props.id)
    const [lastFetchTypeInternal, setLastFetchTypeInternal] = React.useState(props.lastFetchType)

    var chartRef: React.RefObject<Line> = React.createRef<Line>()

    var refs: React.RefObject<DataDropdown>[] = []
    const dateFormat = 'YYYY-MM-DD'
    const [fromSelected, setFromSelected] = React.useState(moment(new Date()).format(dateFormat))
    const [toSelected, setToSelected] = React.useState(moment(new Date()).format(dateFormat))


    const dropdowns = props.dropdowns.map((element, index) => {
        let ref = React.createRef<DataDropdown>()
        refs.push(ref)
        return (
            <div key={index} className="d-flex justify-content-center">
                <DataDropdown
                    chartId={id}
                    ref={ref}
                    index={index}
                    chartDataColors={props.chartDataColors}
                    options={element.options}
                    allDropdownsSelected={props.selected}
                    title={element.selected}
                    lastFetchType={props.lastFetchType}
                    fromSelected={fromSelected}
                    toSelected={toSelected}
                    selected={element.selected}
                    dropdownSelect={props.dropdownSelect}
                    fetch={props.fetch}
                />
            </div>
        )
    })

    return (

        <Container fluid className="margin-top-20">
            <Row className='d-flex justify-content-center'>
                <Col>
                    <Button color='warning' block={true} onClick={
                        () => {
                            const currentChart = chartRef.current.chartInstance
                            //@ts-ignore 
                            currentChart.resetZoom()
                        }}>
                        RESET ZOOM
                    </Button>
                </Col>
                <Col>
                    <ColorSettings
                        chartDataColors={props.chartDataColors}
                        chartId={id}
                        allDropdownsSelected={props.selected}
                        lastFetchType={props.lastFetchType}
                        fromSelected={fromSelected}
                        toSelected={toSelected}
                        colors={props.chartDataColors}
                        changeDataColor={props.changeDataColor}
                        fetch={props.fetch}
                    />
                </Col>
            </Row>
            <Row>
                <Line ref={chartRef} data={props.data} options={props.options}></Line>
            </Row>
            <Row className='d-flex justify-content-center'>
                <ButtonGroup className='btngroup-minimized'>
                    <Button color='danger' onClick={() => {
                        props.removeDropdown(id)
                        props.fetch(id, props.selected, props.lastFetchType, props.chartDataColors, fromSelected, toSelected)
                    }} >-</Button>
                    {dropdowns}
                    <Button color='success' onClick={() => {
                        props.addDropdown(id)
                        props.fetch(id, props.selected, props.lastFetchType, props.chartDataColors, fromSelected, toSelected)
                    }}>+</Button>
                </ButtonGroup>
            </Row>
            <Row className='mt-3'>
                <Col md>
                    <DateTime
                        dateFormat='YYYY-MM-DD'
                        timeFormat={false}
                        initialValue={new Date()}
                        onChange={(moment: moment.Moment) => setFromSelected(moment.format(dateFormat))}
                    />
                </Col>
                <Col md>
                    <Button
                        color='warning'
                        block={true}
                        onClick={() => {
                            props.setLastFetchType(id, GET_FROM_TO)
                            props.fetch(id,
                                [] = props.dropdowns.map(dropdown => { return dropdown.selected }),
                                'GET_FROM_TO',
                                props.chartDataColors,
                                fromSelected,
                                toSelected
                            )
                        }}>
                        SHOW
                    </Button>
                </Col>
                <Col md>
                    <DateTime
                        dateFormat='YYYY-MM-DD'
                        timeFormat={false}
                        initialValue={new Date()}
                        onChange={(moment: moment.Moment) => setToSelected(moment.format(dateFormat))}
                    />
                </Col>
            </Row>
            <Row className="mt-3">
                <Col md>
                    <Button
                        color='primary'
                        onClick={
                            () => {
                                props.setLastFetchType(id, GET_LAST_60)
                                props.fetch(id, [] = props.dropdowns.map(dropdown => dropdown.selected), 'GET_LAST_60', props.chartDataColors)
                            }
                        }
                        block={true} >
                        LAST 60 MINUTES
                    </Button>
                </Col>
                <Col md>
                    <Button
                        color='primary'
                        onClick={
                            () => {
                                props.setLastFetchType(id, GET_LAST_6)
                                props.fetch(id, [] = props.dropdowns.map(dropdown => dropdown.selected), 'GET_LAST_6', props.chartDataColors)
                            }
                        }
                        block={true}>
                        LAST 6 HOURS
                    </Button>
                </Col>
                <Col md>
                    <Button
                        color='primary'
                        onClick={
                            () => {
                                props.setLastFetchType(id, GET_LAST_DAY)
                                props.fetch(id, [] = props.dropdowns.map(dropdown => dropdown.selected), 'GET_LAST_DAY', props.chartDataColors)
                            }
                        }
                        block={true}>
                        LAST 24 HOURS
                    </Button>
                </Col>
                <Col md>
                    <Button
                        color='primary'
                        onClick={
                            () => {
                                props.setLastFetchType(id, GET_LAST_WEEK)
                                props.fetch(id, [] = props.dropdowns.map(dropdown => dropdown.selected), 'GET_LAST_WEEK', props.chartDataColors)
                            }
                        }
                        block={true}>
                        LAST WEEK
                    </Button>
                </Col>
            </Row>
        </Container >
    )
}
