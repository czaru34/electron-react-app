import { Data } from 'popper.js'
import * as React from 'react'
import { Dropdown, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap'
import * as rgb from '../helpers/colors'

namespace DataDropdown {
    export interface Props {
        chartId: number
        index?: number
        title: string
        selected: string
        allDropdownsSelected: string[]
        chartDataColors: rgb.chartDataColor[]
        lastFetchType: string
        options: string[]
        dropdownOpen?: boolean
        fromSelected: string
        toSelected: string
        dropdownSelect: (id: number, index: number, payload: string) => void
        fetch: (id: number, selected: string[], typeOfFetch: string, chartDataColors: rgb.chartDataColor[], from?: string, to?: string) => void
    }
    export interface State {
        chartId: number
        index?: number
        title: string
        selected: string
        options: string[]
        dropdownOpen?: boolean
    }
}

export default class DataDropdown extends React.Component<DataDropdown.Props, DataDropdown.State>
{
    constructor(props: any) {
        super(props)
        this.state = {
            chartId: props.chartId,
            index: props.index,
            title: props.selected,
            selected: props.selected,
            options: props.options,
            dropdownOpen: false
        }
    }

    componentDidMount() {
        console.log('MyDropdown has mounted!')
        console.log(this.state.options)
        this.setState({ selected: this.props.selected })
    }

    getSelected() {
        return this.state.selected
    }

    getTitle() { return this.state.title }

    getIndex() { return this.state.index }

    handleSelect = (eventKey: any) => {
        console.log(`Event: ${eventKey.target.value}`)
        var val: string = eventKey.target.value
        var checkedOption = this.props.options.filter((option) => { return option === val })

        console.log(checkedOption[0])

        if (checkedOption[0].length > 0) {
            this.props.dropdownSelect(this.state.chartId,
                this.state.index,
                checkedOption[0])
            this.setState({ title: checkedOption[0], selected: checkedOption[0] })
            this.props.fetch(this.props.chartId, this.props.allDropdownsSelected, this.props.lastFetchType, this.props.chartDataColors, this.props.fromSelected, this.props.toSelected)
        }

    }

    makeItem = (item: string) => {
        return <DropdownItem key={item} onClick={this.handleSelect}>{item.valueOf()}</DropdownItem>
    }

    toggle = () => {
        let isOpen = !(this.state.dropdownOpen)
        this.setState({ dropdownOpen: isOpen })
    }

    render() {
        return (
            <ButtonDropdown
                direction="up"
                variant="dark"
                isOpen={this.state.dropdownOpen}
                toggle={this.toggle.bind(this)}>
                <DropdownToggle caret color='secondary'>
                    {this.state.title}
                </DropdownToggle>
                <DropdownMenu>
                    {
                        this.props.options.map((option, index) => <DropdownItem key={index} value={option} onClick={this.handleSelect.bind(this)}>{option.valueOf()}</DropdownItem>)
                    }
                </DropdownMenu>
            </ButtonDropdown>
        )
    }
}