import { Nav, NavItem, Button } from 'reactstrap'

import {LinkContainer} from 'react-router-bootstrap'

import * as React from 'react'

export default class MyNav extends React.Component {
    render() {
        return (
            <Nav> 
                <NavItem>
                    <LinkContainer to="/status">
                        <Button size="lg">
                            status
                        </Button>
                    </LinkContainer>
                </NavItem>
                <NavItem>
                <LinkContainer to="/samples">
                        <Button size="lg">
                            samples
                        </Button>
                    </LinkContainer>
                </NavItem>
                <NavItem>
                    <LinkContainer to="/chart">
                        <Button size="lg">
                            charts
                        </Button>
                    </LinkContainer>
                </NavItem>                
            </Nav>
        )
    }
}