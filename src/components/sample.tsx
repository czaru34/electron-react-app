import * as React from 'react'
import axios from 'axios'

import { SampleList } from './sample-list'
import { address } from 'ip'
import { Row, Col } from 'reactstrap'


export const Samples = () => {

    const [timestamp, setTimestamp] = React.useState(new Date().toLocaleString())
    const [Temperature, setTemperature] = React.useState('')
    const [Humidity, setHumidity] = React.useState('')
    const [Pm10, setPm10] = React.useState('')
    const [Pm25, setPm25] = React.useState('')
    const [Pm100, setPm100] = React.useState('')
    const [Particles03, setParticles03] = React.useState('')
    const [Particles05, setParticles05] = React.useState('')
    const [Particles10, setParticles10] = React.useState('')
    const [Particles25, setParticles25] = React.useState('')
    const [Particles50, setParticles50] = React.useState('')
    const [Particles100, setParticles100] = React.useState('')

    const [samples, setSamples] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    React.useEffect(() => {
        fetchSamples()
    }, [])

    const fetchSamples = async () => {
        axios
            .get('http://' + address() + ':80/fetch/all')
            .then(response => {
                console.log(response.data)
                setSamples(response.data)
                setLoading(false)
            })
            .catch(error => console.error(`There was an error retrieving the sample list: ${error}`))
    }

    return (
        <SampleList samples={samples} loading={loading} />
    )
}