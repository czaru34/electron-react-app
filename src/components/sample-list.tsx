import * as React from 'react'

import { SampleListRow } from './sample-list-row'

import { Table, Row, Col, Container } from 'reactstrap'

interface SampleUI {
    timestamp: string
    Temperature: number
    Humidity: number
    Pm10: number
    Pm25: number
    Pm100: number
    Particles03: number
    Particles05: number
    Particles10: number
    Particles25: number
    Particles50: number
    Particles100: number
}

interface SampleListUI {
    samples: SampleUI[];
    loading: boolean;
}

export const SampleList = (props: SampleListUI) => {

    if (props.loading)
        return <p>Table is loading...</p>

    return (
        <Container fluid className="margin-zero-auto">
            <Table bordered={true} dark={true} size="lg">
                <thead>
                    <tr>
                        <th className='bigger-text'>Timestamp</th>
                        <th className='bigger-text'>Temperature [°C]</th>
                        <th className='bigger-text'>Humidity [%]</th>
                        <th className='bigger-text'>Pm10 [μg/m³]</th>
                        <th className='bigger-text'>Pm25 [μg/m³]</th>
                        <th className='bigger-text'>Pm100 [μg/m³]</th>
                        <th className='bigger-text'>Particles03 [number]</th>
                        <th className='bigger-text'>Particles05 [number]</th>
                        <th className='bigger-text'>Particles10 [number]</th>
                        <th className='bigger-text'>Particles25 [number]</th>
                        <th className='bigger-text'>Particles50 [number]</th>
                        <th className='bigger-text'>Particles100 [number]</th>
                    </tr>
                </thead>
                <tbody>
                    {props.samples.length > 0 ? (
                        props.samples.map((sample: SampleUI, idx) => (
                            <SampleListRow
                                key={sample.timestamp}
                                sample={sample}
                                position={idx + 1}
                            />
                        )
                        )
                    ) : (
                            <tr >
                                <td style={{ textAlign: 'center' }} colSpan={5}>There are no samples to show.</td>
                            </tr>
                        )
                    }
                </tbody>
            </Table>
        </Container>
    )
}