import * as React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { Row } from 'reactstrap'

import MyNav from './my-nav'
import { Samples } from './sample'
import { StatusContainer } from '../containers/appstatus'
import { ChartsContainer } from '../containers/charts'

import { applyMiddleware, createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import allReducer from '../reducers/allReducers'
import thunk from 'redux-thunk'

export type AppState = ReturnType<typeof allReducer>
let store = createStore(allReducer, compose(applyMiddleware(thunk)))


console.log(store.getState())

export default class Main extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Row className="justify-content-center margin-zero-auto nav-color">
                        <MyNav />
                    </Row>
                    <Switch>
                        <Route exact path='/status' component={StatusContainer} />
                        <Route path='/samples' component={Samples} />
                        <Route path='/chart' component={ChartsContainer} />
                        <Route render={() => <Redirect to="/status" />} />
                    </Switch>
                </Router>
            </Provider>
        )
    }
}

