import * as React from 'react'
import { Container, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { ChromePicker } from 'react-color'
import { FiSettings } from 'react-icons/fi'
import DataDropdown from './datadropdown'
import * as rgb from '../helpers/colors'

namespace ColorSettings {
	export interface Props {
		chartId: number
		allDropdownsSelected: string[]
		chartDataColors: rgb.chartDataColor[]
		lastFetchType: string
		fromSelected: string
		toSelected: string
		colors: rgb.chartDataColor[]
		changeDataColor: (color: rgb.chartDataColor) => void
		fetch: (id: number, selected: string[], typeOfFetch: string, chartDataColors: rgb.chartDataColor[], from?: string, to?: string) => void
	}
	export interface State {
		show: boolean,
		color: string,
		currentDataType: string
	}
}

export default class ColorSettings extends React.Component<ColorSettings.Props, ColorSettings.State>
{
	constructor(props: any) {
		super(props)
		this.state = {
			show: false,
			color: '',
			currentDataType: 'Temperature'
		}
	}

	componentDidMount() {
		this.handleSave.bind(this)
	}

	toggle = () => {
		let newShow = !this.state.show
		this.setState({
			show: newShow
		})
	}

	handleSave = () => {
		this.props.fetch(this.props.chartId, this.props.allDropdownsSelected, this.props.lastFetchType, this.props.chartDataColors, this.props.fromSelected, this.props.toSelected)
	}

	handleChangeComplete = (color: any) => {
		this.setState({ color: color })
		this.props.changeDataColor({
			dataType: this.state.currentDataType,
			color: color.rgb
		})
	}
	handleCurrentDataType = (dataType: string) => {
		this.setState({
			currentDataType: dataType
		})
	}

	render() {
		return (
			<Container>
				<Button color='warning' block={true} onClick={this.toggle}>
					COLOR SETTINGS <FiSettings />
				</Button>
				<Modal
					isOpen={this.state.show}
					toggle={this.toggle}
					backdrop={false}
					keyboard={false}
				>
					<ModalHeader>
						Color settings
                     </ModalHeader>
					<ModalBody>
						<Container>
							<div className="d-flex justify-content-between">
								<div className="mt-3">
									{
										this.props.colors.map((chartColor, index) =>
											<button
												key={chartColor.dataType}
												value={rgb.getRgbaString(chartColor.color)}
												style={{ background: rgb.getRgbaString(chartColor.color) }}
												onClick={() => this.handleCurrentDataType(chartColor.dataType)}
											>
												{chartColor.dataType}
											</button>)
									}
								</div>
								<div className="mt-3">
									<ChromePicker disableAlpha={true} color={this.state.color} onChangeComplete={this.handleChangeComplete} />
								</div>
							</div>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button color="danger" onClick={this.toggle}>
							Go back
                         </Button>
						<Button color="success" onClick={this.handleSave}>
							Save
                         </Button>
					</ModalFooter>
				</Modal>
			</Container>
		)
	}
}
