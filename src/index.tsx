import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {createStore} from 'redux'


import Main from './components/main'

import './styles/styles.css'

const rootElement = document.getElementById('app')



ReactDOM.render(<Main/>, rootElement)