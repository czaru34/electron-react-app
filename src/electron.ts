// src/electron.js
import { app, BrowserWindow, session } from 'electron'
import * as path from 'path'
import * as App from './server/app'
import * as ip from 'ip'
import { SampleController } from './server/controllers/sample.controller'

const application: App.App = new App.App(
    [new SampleController()],
    80,
    ip.address()
)

console.log(ip.address())

application.listen()

app.on('ready', async () => {

    let win = new BrowserWindow({
        width: 1280,
        height: 854,
        webPreferences: {
            nodeIntegration: true,
            worldSafeExecuteJavaScript: true,
            webSecurity: false,
            allowRunningInsecureContent: true
        }
    })
    await win.loadFile('index.html')
    win.show()

    //win.webContents.openDevTools()
})



