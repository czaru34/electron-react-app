const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const { resolve } = require('path')

module.exports = [
    {
        mode: 'development',
        entry: './src/electron.ts',
        target: 'electron-main',
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
        },
        module: {
            rules: [{
                test: /\.ts$/,
                include: /src/,
                use: [{ loader: 'ts-loader' }]
            }],

        },
        output: {
            path: __dirname + '/dist',
            filename: 'electron.js'
        },
        externals: {
            'knex': 'commonjs knex',
            //'sqlite3': 'commonjs sqlite3',
            'fs': 'commonjs fs'
        }
    },
    {
        mode: 'development',
        entry: './src/index.tsx',
        target: 'electron-renderer',
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
        },
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.ts(x?)$/,
                    include:
                        [
                            path.resolve(__dirname, 'src'),
                            path.resolve(__dirname, 'src/components')
                        ],
                    use: [{ loader: 'ts-loader' }]
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                }
            ]
        },
        output: {
            path: __dirname + '/dist',
            filename: 'index.js',
            sourceMapFilename: 'index.js.map'
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html'
            })
        ]
    }
]